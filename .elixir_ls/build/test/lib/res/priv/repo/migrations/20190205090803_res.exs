defmodule Res.Repo.Migrations.Res do
  use Ecto.Migration

  def change do
    create table(:plant) do
      add :name, :string
      add :description, :string
      add :price, :float
      timestamps()
    end
  end
end
