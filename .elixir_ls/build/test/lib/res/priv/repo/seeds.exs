# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Res.Repo.insert!(%Res.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Res.DataAccess
import Ecto.Query, warn: false


plant = [
 %{name: "Excavotor", description: "15 Tonne Mini excavotor", price: 150.00},
 %{name: "Excavotor", description: "3 Tonne Mini excavotor", price: 150.00},
 %{name: "Excavotor", description: "5 Tonne Mini excavotor", price: 150.00},
 %{name: "Excavotor", description: "8 Tonne Mini excavotor", price: 150.00}

]

Enum.each(plant, fn(data) ->
DataAccess.create_plant(data)
end)
