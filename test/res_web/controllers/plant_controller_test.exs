defmodule ResWeb.PlantControllerTest do
  use ResWeb.ConnCase

  test "Available for rental Period", %{conn: conn} do
    [%{start_date: "12-01-2016", end_date: "14-01-2016", user_id: 2, plant_id: 2}]
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    conn = get conn, "/show", %{}
    plant_list = Repo.all(Plan)
    conn = post conn, "/", %{plant: [plant: 1]}
    assert html_response(conn, 200) =~ ~r/Available/
  end


  test "Unavailable for rental Period", %{conn: conn} do
    [%{start_date: "12-01-2016", end_date: "14-01-2016", user_id: 2, plant_id: 2}]
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    conn = get conn, "/show", %{}
    plant_list = Repo.all(Plan)
    conn = post conn, "/", %{plant: [plant: 1]}
    assert html_response(conn, 200) =~ ~r/Unavailable/
  end

end
