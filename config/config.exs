# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :res,
  ecto_repos: [Res.Repo]

# Configures the endpoint
config :res, ResWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "hWKHP1xrCQT0T7VtTtX5m8UKVUXVd17Ltg68tNd2QY0UUeO2c7Ii9ZYCV90klnQ3",
  render_errors: [view: ResWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Res.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
