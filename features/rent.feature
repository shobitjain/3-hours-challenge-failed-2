Scenario: Retrieve the list of plants
    Given the following plants are available
        | Name     | Description             | Price |Action |
        | Excavotor| 15 Tonne Mini excavotor | 150.00| Select|
        | Excavotor|  3 Tonne Mini excavotor | 200.00| Select|
        | Excavotor|  5 Tonne Mini excavotor | 250.00| Select|
        | Excavotor|  8 Tonne Mini excavotor | 300.00| Select|
    And I want to hire "Excavotor" with date "12-01-2016" to "12-01-2017"
    And I enter the details to search field
    When I submit the request
    Then I should receive a list of available plants

Scenario: Retrieve the single plant (plant rental)
    Given the following plants are available
        | Name     | Description             | Price |Action |
        | Excavotor| 15 Tonne Mini excavotor | 150.00| Select|
        | Excavotor|  3 Tonne Mini excavotor | 200.00| Select|
        | Excavotor|  5 Tonne Mini excavotor | 250.00| Select|
        | Excavotor|  8 Tonne Mini excavotor | 300.00| Select|
    And I want a "select" a plant
    When I select a plant
    Then I should receive details of purchase order