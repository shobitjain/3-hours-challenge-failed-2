defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    %{}
  end
  scenario_finalize fn _status, _state ->
    nil
    #Hound.end_session
  end
  given_ ~r/^the following plants are available$/, fn state ->
    navigate_to "/"
    {:ok, state}
  end
  and_ ~r/^I want to hire "(?<name>[^"]+)" with date "(?<start_date>[^"]+)" to "(?<end_date>[^"]+)"$/,
  fn state, %{name: name,start_date: start_date,end_date: end_date} ->
    {:ok, state}
  end
  and_ ~r/^I enter the details to search field$/, fn state ->
    click({:id, "submit_button"})
    {:ok, state}
  end
  when_ ~r/^I submit the request$/, fn state ->
    {:ok, state}
  end
  then_ ~r/^I should receive a list of available plants$/, fn state ->
    navigate_to "/list"
    {:ok, state}
  end
  and_ ~r/^I want a "(?<argument_one>[^"]+)" a plant$/,
  fn state, %{argument_one: _argument_one} ->
    {:ok, state}
  end
  when_ ~r/^I select a plant$/, fn state ->
    {:ok, state}
  end
  then_ ~r/^I should receive details of purchase order$/, fn state ->
    navigate_to "/list"
    {:ok, state}
  end

end
