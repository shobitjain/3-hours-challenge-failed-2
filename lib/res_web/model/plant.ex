defmodule Res.Model.Plant do
  use Ecto.Schema
  import Ecto.Changeset


  schema "plant" do
    field :name, :string
    field :description, :string
    field :price, :float
    timestamps()
  end

  @doc false
  def changeset(plant, attrs) do
    plant
    |> cast(attrs, [:name, :description, :price])
    |> validate_required([:name, :description, :price])
  end
end
