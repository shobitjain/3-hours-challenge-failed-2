defmodule ResWeb.PlantController do
  use ResWeb, :controller
  alias Res.DataAccess
  alias Res.Repo
  import Ecto.Query, only: [from: 2]
  alias Res.Model.Plant

  def form(conn, _params) do
    render conn, "form.html"
  end
  def show(conn, _params) do
    plant_list = Repo.all(Plant)
    render(conn, "show.html", plant_list: plant_list)
  end
end
