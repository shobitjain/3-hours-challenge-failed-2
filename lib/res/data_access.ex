defmodule Res.DataAccess do
  @moduledoc """
  The Accounts context.
  """

  alias Res.Repo

  # alias Vueshatter.Models.LoanHistory
  alias  Res.Model.Plant

  def create_plant(attrs \\ %{}) do
    %Plant{}
    |> Plant.changeset(attrs)
    |> Repo.insert()
  end
end
